**Azure Container Blobs Listing through HTTP using Azure Storage JavaScript Library**

This repository provides a solution to lists all blobs in a container with directory-level support using a combination of the newly launched Static website feature and the Azure Storage JavaScript Library.

The Static website container a.k.a. `$web` container is used to store the required HTML file and the JS Library to lists all the Blobs from another container and display it through the Web (HTTP).

---

## Enable and Configure Static Website Feature

By default, Static Website option is not enabled on Azure.

To enable the option:

1. Go to Azure Portal and click the `Storage accounts` menu.
2. Click the Storage account name.
3. Under `Settings`, click `Static website (preview)`.
4. Switch it to `Enabled`.
5. Take note of your `Primary endpoint`.
6. Under `Index document name` and `Error document path`, put `index.html`.
7. Click `Save`.

---

## Add the CORS Rule

CORS is an HTTP feature that enables a web application running under one domain to access resources in another domain.

Web browsers implement a security restriction known as same-origin policy that prevents a web page from calling APIs in a different domain.

CORS provides a secure way to allow one domain (the origin domain) to call APIs in another domain.

CORS rule is required for the JavaScript function to make a successful call to the Storage APIs.

To create the CORS rule:

1. Go to Azure Portal and click the `Storage accounts` menu.
2. Click the Storage account name.
3. Under `Settings`, click `CORS`.
4. Under `Blob service`, create the rule with the following values:

	*change the value according to your own preferences*
	
	* ALLOWED ORIGINS: *
	* ALLOWED METHODS: DELETE, GET, HEAD, MERGE, POST, OPTIONS, PUT
	* ALLOWED HEADERS: *
	* EXPOSED HEADERS: *
	* MAX AGE: 86400
	
5. Click `Save`.

---

## Configure the JavaScript Function

The function needs to be configured with a correct value first before uploaded to the `$web` container.

To configure the function, do an edit to `index.html` file using your preffered editor with the following changes:

- Change `[PAGE_TITLE]` in Line 6 with the preferred value of the page title you want to display in the browser. *(e.g. HySDS Dataset Files)*
- Change `[STATIC_WEBSITE_PRIMARY_ENDPOINT]/[CONTAINER_NAME]/` in Line 17 with the endpoint value obtained from the previous step and the container name you want to list the blobs within. *(e.g. https://hysds3.z23.web.core.windows.net/dataset/)*
- Change `[PRIMARY_BLOB_SERVICE_ENDPOINT]` in Line 59 with the value of your Blob Endpoint **without trailing slash**. *(e.g. https://hysds3.blob.core.windows.net)*
- Change `[STATIC_WEBSITE_PRIMARY_ENDPOINT]` in Line 60 with the endpoint value obtained from the previous step **without trailing slash**. *(https://hysds3.z23.web.core.windows.net)*
- Change `[CONTAINER_NAME]` in Line 61 with the container name you want to list the blobs within. *(e.g. dataset)*
- Save the file.

---

## Upload Files to the Static Website Container

Now you should have the following 3 files in your system:

- `index.html` which has been modified with the correct value.
- `azure-storage.blob.min.js` and
- `azure-storage.common.min.js`

Upload those files to the Static website container *($web)* with your preferred methods.

Using Azure Portal:

1. Go to Azure Portal and click the `Storage accounts` menu.
2. Click the Storage account name.
3. Under `Blob service`, click `Blobs`.
4. Click `$web`.
5. Upload all the files using the `Upload` button.

Using Microsoft Azure Storage Explorer:

1. Go to the `$web` container under `Blob Containers` under your storage account.
2. Click `Upload` > `Upload Files...` > then select all the files.
3. Click `Upload`

---

## Verify the Function

Open `[STATIC_WEBSITE_PRIMARY_ENDPOINT]/[CONTAINER_NAME]/` *(e.g. https://hysds3.z23.web.core.windows.net/dataset/)* using your preffered browser to confirm that the listing function is working as expected.

If there is no Blobs listed, please re-check everything again and see whether you missed any of the previous setup.

---

## Using it as part of the HySDS Cluster System

Current limitation of the Static website feature on Azure is that it cannot be applied to an existing container *(e.g. code/dataset)* at least at the time of writing.

Instead, it creates a new, specifically designed for this feature a.k.a. `$web` container.

This make the HySDS system must be configured with two different URL types with different purposes:

- URL with `[PRIMARY_BLOB_SERVICE_ENDPOINT]` is used to download the Blobs from **but cannot do the listing**, and
- URL with `[STATIC_WEBSITE_PRIMARY_ENDPOINT]` is used to do the listing purposes **but cannot download the Blobs from**.

	*During the Cluster Setup of HySDS using `sds configure` command, you need to keep `AZURE_WEBSITE_ENDPOINT` parameter to use `[PRIMARY_BLOB_SERVICE_ENDPOINT]` (\*.blob.core.windows.net) even though you already have `[STATIC_WEBSITE_PRIMARY_ENDPOINT]` enabled. This is because some part of the HySDS tries to download some files from the HTTP protocol on `WEBSITE_ENDPOINT` which is not possible.* 

To make the listing works with the JavaScript function in `$web` container, you need to redirect the Browse button URL to the `$web` container using the following method:

1. Log into GRQ instance and change to this directory: `/home/ops/sciflo/ops/tosca/tosca/services`
2. Edit `dataset.py` with your preffered editor and go to Line 37
3. Replace `redirect(url)` with `redirect(url.replace("blob", "z23.web"))`. *(replace `z23.web` with the sub-domain of your own `[STATIC_WEBSITE_PRIMARY_ENDPOINT]`)*
4. Save the changes
5. Restart tosca using this command: `supervisorctl restart tosca`

---